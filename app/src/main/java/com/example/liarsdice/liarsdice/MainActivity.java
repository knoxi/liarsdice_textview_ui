package com.example.liarsdice.liarsdice;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    //    initial BID variables for
    int numDice;
    int diceVal;
    int lastBid_numDice = 0;
    int lastBid_diceVal = 0;

    // player names
    String[] playerId = {"Mike", "Tony", "Mark", "Darren"};


    int idPlayer0Dice[] = {5, 3, 4, 4, 5};
    int idPlayer1Dice[] = {2, 2, 2, 2, 2};
    int idPlayer2Dice[] = {4, 4, 1};
    int idPlayer3Dice[] = {1, 2, 3, 6, 6};


    // These represent the number of dice each player is holding
    int dicePerPlayer0 = idPlayer0Dice.length;
    int dicePerPlayer1 = idPlayer1Dice.length;
    int dicePerPlayer2 = idPlayer2Dice.length;
    int dicePerPlayer3 = idPlayer3Dice.length;

    int[] dicePerPlayer = {dicePerPlayer0, dicePerPlayer1, dicePerPlayer2, dicePerPlayer3};

    int eventCount = 2;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayPlayerId();
        displayDicePerPlayer();
        diceInPlay();
        displayMessage();
        displayPlayersDice();
    }

    public void diceInPlay(){
        int diceInPlay = 0;
        for (int i : dicePerPlayer) {
            diceInPlay += i;
        }
        displayDiceInPlay(diceInPlay);
    }
    public void displayDicePerPlayer() {
        int count = 0;
        int txtIds[] = {R.id.playersDice1, R.id.playersDice2, R.id.playersDice3, R.id.playerDice4};
        for (int i = 0; i < dicePerPlayer.length; i++){
            for (int j = count; j == count; j++) {
                TextView dice = (TextView) findViewById(txtIds[j]);
                dice.setText(""+dicePerPlayer[i]);

            }
            count++;
        }
    }


    // Display all the players names
    public void displayPlayerId() {
        int count = 0;
        int txtIds[] = {R.id.Player1, R.id.Player2, R.id.Player3, R.id.Player4};

        for (int i = 0; i < playerId.length; i++){
            for (int j = count; j == count; j++) {
                TextView playerName = (TextView) findViewById(txtIds[j]);
                playerName.setText(playerId[i]);
            }

            count++;
        }

    }

    public void displayPlayersDice() {
        int count = 0;
        TextView title = (TextView) findViewById(R.id.PlayersDice);
        title.setText(playerId[eventCount] + "'s Dice");

        int txtId[] = {R.id.Dice1, R.id.Dice2, R.id.Dice3, R.id.Dice4, R.id.Dice5};

        if (eventCount == 0)
            for (int i = 0; i < idPlayer0Dice.length; i++) {
                for (int j = count; j == count; j++) {
                    TextView num = (TextView) findViewById(txtId[j]);
                    num.setText("" + idPlayer0Dice[i]);
                }
                count++;
            }
        else if (eventCount == 1)
            for (int i = 0; i < idPlayer1Dice.length; i++) {
                for (int j = count; j == count; j++) {
                    TextView num = (TextView) findViewById(txtId[j]);
                    num.setText("" + idPlayer1Dice[i]);
                }
                count++;
            }
        else if (eventCount == 2)
            for (int i = 0; i < idPlayer2Dice.length; i++) {
                for (int j = count; j == count; j++) {
                    TextView num = (TextView) findViewById(txtId[j]);
                    num.setText("" + idPlayer2Dice[i]);
                }
                count++;
            }
        else if (eventCount == 3)
            for (int i = 0; i < idPlayer3Dice.length; i++) {
                for (int j = count; j == count; j++) {
                    TextView num = (TextView) findViewById(txtId[j]);
                    num.setText("" + idPlayer3Dice[i]);
                }
                count++;
            }


        /*
        TextView num = (TextView) findViewById(R.id.Dice1);
        num.setText(""+i);*/
    }





    // display the total dice in play
    private void displayDiceInPlay(int i) {
        TextView diceInPlay = (TextView) findViewById(R.id.DiceInPlay);
        diceInPlay.setText("Dice In Play " + i);
    }

    // display info to the message board
    private void displayMessage() {
        String message = (playerId[eventCount] + "'s Turn To Bid");
        TextView msg = (TextView) findViewById(R.id.Info_Out);
        msg.setText(message);
    }

    // Method to increment the number of dice bid
    public void increaseDiceNum(View view) {
        numDice += 1;
        displayNumDice(numDice);
    }

    // Method to decrement the number of the dice Bid
    public void decreaseNumDice(View view) {
        numDice -= 1;
        displayNumDice(numDice);
    }

    // Display the Dice number to Bid
    private void displayNumDice(int i) {
        TextView newBid = (TextView) findViewById(R.id.newBid_NumDice);
        newBid.setText("" + i);
    }

    // Method to increment the Dice Value Bid
    public void increaseDiceValue(View view) {
        diceVal += 1;
        displayDiceValue(diceVal);
    }

    // Method to decrease the Dice Value Bid
    public void DecreaseDiceVal(View view) {
        diceVal -= 1;
        displayDiceValue(diceVal);
    }

    // Display the Dice Value to Bid
    private void displayDiceValue(int i) {
        TextView newBid = (TextView) findViewById(R.id.newBid_DiceVal);
        newBid.setText("" + i);
    }

    // Method to reset bids to last Bid dice Values
    public void resetBids(View view) {
        numDice = lastBid_numDice;
        diceVal = lastBid_diceVal;
        displayNumDice(numDice);
        displayDiceValue(diceVal);

    }

    public void callLiar(View view) {
        String Liar = (playerId[eventCount] + " has declared Liar!");
        displayLiarMsg(Liar);
        addEvent();

    }

    private void displayLiarMsg(String message) {
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void placeBid(View view) {
        String Bid = (playerId[eventCount] + " has Bid " + numDice + "x" + diceVal + "'s");
        displayBidMsg(Bid);
        addEvent();
    }

    private void displayBidMsg(String message) {
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public int addEvent(){
        eventCount +=1;
        return eventCount;

    }


}
